(function( root, $, undefined ) {
	"use strict";

	$(function () {
        $(document).ready(function(){
            $(".slick-carousel").slick({
                dots: false,
                infinite: true,
                speed: 300,
                slidesToShow: 1,
                adaptiveHeight: true,
                prevArrow: "<img src='/app/themes/src/img/carousel/prev.png' class='prev-ico' alt='' />",
                nextArrow: "<img src='/app/themes/src/img/carousel/next.png' class='next-ico' alt='' />"
            });

            // Target your .container, .wrapper, .post, etc.

            /* Telestration */

            $(".telestration").on("click",function(ev){
                ev.preventDefault();
                var modalTarget = "#"+$(this).data("tele");
                $(modalTarget).addClass("show");
            });

            $(".telestration-close").on("click",function(ev){
                ev.preventDefault();
                $(".telestration-overlay").removeClass("show");
                $("iframe").attr("src", $("iframe").attr("src"));
            });

            $(".hamburger").on("click",function(){
                $(this).toggleClass("is-active");
            });


        });
	});

} ( this, jQuery ));