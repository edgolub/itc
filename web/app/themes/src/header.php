<!doctype html>
<html <?php language_attributes(); ?> class="no-js">
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>">
		<title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>

		<link href="//www.google-analytics.com" rel="dns-prefetch">
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/icons/favicon.ico" rel="shortcut icon">
		<link href="<?php echo esc_url( get_template_directory_uri() ); ?>/img/icons/touch.png" rel="apple-touch-icon-precomposed">
		<link rel="alternate" type="application/rss+xml" title="<?php bloginfo( 'name' ); ?>" href="<?php bloginfo( 'rss2_url' ); ?>" />

		<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php bloginfo( 'description' ); ?>">
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

        <script src="https://cdnjs.cloudflare.com/ajax/libs/fitvids/1.2.0/jquery.fitvids.min.js"></script>

        <link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick.css"/>
        <!-- Add the slick-theme.css if you want default styling -->
        <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.9.0/slick-theme.css"/>

        <?php wp_head(); ?>
        <!--[if IE]>
        <link rel="stylesheet" type="text/css" href="/web/app/themes/src/css/iefix.css" />
        <![endif]-->
        <style>
            @media all and (-ms-high-contrast: none), (-ms-high-contrast: active) {
                .topbar-rightside > .row {
                    display: block !important;
                }

                .header .navbar-mainhdr .navbar-nav {
                    display: block !important;
                }
                .header .navbar-mainhdr .navbar-nav .nav-item {
                    padding: 0 18px !important;
                }
            }
        </style>
        <style>
            @supports (-ms-accelerator:true) {
                .topbar-rightside > .row {
                    display: block !important;
                }

                .header .navbar-mainhdr .navbar-nav {
                    display: block !important;
                }
                .header .navbar-mainhdr .navbar-nav .nav-item {
                    padding: 0 18px !important;
                }
            }
        </style>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">


		<!--<script>
		// conditionizr.com
		// configure environment tests
		conditionizr.config({
			assets: '<?php echo esc_url( get_template_directory_uri() ); ?>',
			tests: {}
		});
		</script>-->

	</head>
	<body <?php body_class(); ?>>

		<!-- wrapper -->
		<div class="site-wrapper">

			<!-- header -->
			<header class="header clear" role="banner">

                <div class="header-tophalf">

                    <div class="container">
                        <div class="row">
                            <div class="col col-sm-3">
                                <!-- logo -->
                                <div class="logo">
                                    <a href="<?php echo esc_url( home_url() ); ?>">
                                        <!-- svg logo - toddmotto.com/mastering-svg-use-for-a-retina-web-fallbacks-with-png-script -->
                                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/img/logo.svg" alt="Logo" class="logo-img">
                                    </a>
                                </div>
                                <!-- /logo -->
                            </div>
                            <div class="col col-sm-9 navbar-light topbar-rightside">
                                <div class="row">
                                    <div class="col site-title-cont">
                                        <h2><?php bloginfo( 'name' ); ?></h2>
                                        <h5><?php bloginfo( 'description' ); ?></h5>
                                    </div>
                                    <div class="col-auto toggler-cont">
                                        <button class="navbar-toggler pull-right hamburger hamburger--collapse" type="button" data-toggle="collapse" data-target="#bs4navbar" aria-controls="bs4navbar" aria-expanded="false" aria-label="Toggle navigation">
                                          <span class="hamburger-box">
                                            <span class="hamburger-inner"></span>
                                          </span>
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <nav class="navbar navbar-expand-lg navbar-dark navbar-mainhdr">
                    <div class="container">

                        <div class="collapse navbar-collapse" id="bs4navbar">
                            <?php
                            html5blank_nav( );
                            ?>
                        </div>
                    </div>
                </nav>
					<!-- /nav -->

			</header>
			<!-- /header -->
