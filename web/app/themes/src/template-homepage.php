<?php /* Template Name: HomePage Template */ get_header(); ?>

	<main role="main" aria-label="Content">
		<!-- section -->

		<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>


        <?php if( have_rows('hp_sec1') ): ?>
            <section class="hp-sec1">
                <div class="container">
                    <?php while( have_rows('hp_sec1') ): the_row(); ?>

                        <div class="row">
                            <div class="col-sm-12 col-md">
                                <div class="inner-left-sec">
                                    <?php the_sub_field('hp_sec_content_left'); ?>
                                </div>
                            </div>
                            <div class="col-sm-12 col-md align-self-end">

                                <?php $attachedgrID = 'attaches_linked_files'; ?>
                                <div class="row downbtns-group">

                                    <?php if( have_rows($attachedgrID) ): ?>
                                        <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                            <?php $url = get_sub_field('english_report'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6  col-sm-6">
                                                    <a class="btn downlink" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">English Report</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <?php $url = get_sub_field('arabic_report'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6  col-sm-6">
                                                    <a class="btn downlink" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">Arabic Report</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <?php $url = get_sub_field('english_infographic'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6  col-sm-6">
                                                    <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">English Infographic</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>

                                            <?php $url = get_sub_field('arabic_infographic'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6  col-sm-6">
                                                    <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">Arabic Infographic</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <?php $iframe = get_sub_field('telestration'); ?>
                                            <?php if($iframe): ?>
                                                <div class="col-12">
                                                    <div class="telestration-overlay justify-content-center align-items-center text-center" id="teles-sec1">
                                                        <a class="telestration-close" href="#">x</a>
                                                        <?php echo $iframe; ?>
                                                    </div>
                                                    <a class="telestration mini onpage"  href="#" data-tele="teles-sec1">
                                                        <div class="btn downlink">
                                                            <span class="downlabel">View</span>
                                                            <span class="downdesc">Telestration</span>
                                                        </div>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                    <?php endif; ?>

                                </div>

                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
           </section>
        <?php endif; ?>

        <?php if( have_rows('hp_sec2') ): ?>
            <section class="hp-sec2">
                <div class="container">
                    <?php while( have_rows('hp_sec2') ): the_row(); ?>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="inner-left-sec row">
                                    <div class="col-12 col-lg aside-buttons-col">
                                        <div class="pic-hdr-box">
                                            <?php the_sub_field('leftest_upper'); ?>
                                        </div>
                                        <?php $attachedgrID = 'attaches_linked_files_left'; ?>
                                        <div class="row row-nopads downbtns-group">

                                            <?php if( have_rows($attachedgrID) ): ?>
                                                <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                                    <?php $url = get_sub_field('english_report'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">English Report</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php $url = get_sub_field('arabic_report'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">Arabic Report</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php $url = get_sub_field('english_infographic'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">English Infographic</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php $url = get_sub_field('arabic_infographic'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">Arabic Infographic</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endwhile; ?>
                                            <?php endif; ?>

                                        </div>

                                    </div>
                                    <div class="col-12 col-lg ">
                                        <?php the_sub_field('hp_sec_content_left'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="inner-right-sec row">
                                    <div class="col-12 col-lg  aside-buttons-col">
                                        <div class="pic-hdr-box">
                                            <?php the_sub_field('rightmost_upper'); ?>
                                        </div>
                                        <?php $attachedgrID = 'attaches_linked_files_right'; ?>
                                        <div class="row row-nopads downbtns-group">

                                            <?php if( have_rows($attachedgrID) ): ?>
                                                <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                                    <?php $url = get_sub_field('english_report'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">English Report</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php $url = get_sub_field('arabic_report'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">Arabic Report</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                    <?php $url = get_sub_field('english_infographic'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">English Infographic</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>

                                                    <?php $url = get_sub_field('arabic_infographic'); ?>
                                                    <?php if($url): ?>
                                                        <div class="downlinkgrp col-6 col-sm-6 col-lg-12">
                                                            <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                                <span class="downlabel">Arabic Infographic</span>
                                                                <span class="downdesc">Download</span>
                                                            </a>
                                                        </div>
                                                    <?php endif; ?>
                                                <?php endwhile; ?>
                                            <?php endif; ?>

                                        </div>
                                    </div>
                                    <div class="col-12 col-lg ">
                                        <?php the_sub_field('hp_sec_content_right'); ?>
                                    </div>
                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>
        <section class="sectools">
            <img src="<?php echo the_field('midsec_image'); ?>" alt="" class="img-fluid" />
        </section>



        <?php if( have_rows('hp_sec3') ): ?>
            <section class="hp-sec3">
                <div class="container">
                    <?php while( have_rows('hp_sec3') ): the_row(); ?>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="inner-left-sec">
                                    <div class="content-stub">
                                        <?php the_sub_field('hp_sec_content_left'); ?>
                                    </div>

                                    <?php $attachedgrID = 'attaches_linked_files_left'; ?>
                                    <div class="row downbtns-group">

                                        <?php if( have_rows($attachedgrID) ): ?>
                                            <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                                <?php $url = get_sub_field('english_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('arabic_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('english_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>

                                                <?php $url = get_sub_field('arabic_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        <?php endif; ?>

                                    </div>


                                </div>
                            </div>
                            <div class="col-md-4 wrapper-middle-sec">
                                <div class="inner-middle-sec">
                                    <div class="content-stub">
                                        <?php the_sub_field('hp_sec_content_middle'); ?>
                                    </div>
                                    <?php $attachedgrID = 'attaches_linked_files_middle'; ?>
                                    <div class="row downbtns-group">

                                        <?php if( have_rows($attachedgrID) ): ?>
                                            <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                                <?php $url = get_sub_field('english_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('arabic_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('english_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>

                                                <?php $url = get_sub_field('arabic_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        <?php endif; ?>

                                    </div>

                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="inner-right-sec">

                                    <div class="content-stub">
                                        <?php the_sub_field('hp_sec_content_right'); ?>
                                    </div>

                                    <?php $attachedgrID = 'attaches_linked_files_right'; ?>
                                    <div class="row downbtns-group">

                                        <?php if( have_rows($attachedgrID) ): ?>
                                            <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                                <?php $url = get_sub_field('english_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('arabic_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('english_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>

                                                <?php $url = get_sub_field('arabic_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        <?php endif; ?>

                                    </div>


                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
            </section>
        <?php endif; ?>

        <?php if( have_rows('hp_sec4') ): ?>

            <?php while( have_rows('hp_sec4') ): the_row(); ?>
                <?php $iframe = get_sub_field('telestration_link'); ?>
                <?php if($iframe): ?>
                <section class="telestration-section">
                <div class="container text-center">
                    <div class="col-xs-6 offset-xs-3 col-md-12">
                        <div class="telestration-overlay justify-content-center align-items-center text-center" id="teles-page">
                            <a class="telestration-close" href="#">x</a>
                            <?php echo $iframe; ?>
                        </div>
                        <a class="telestration onpage" href="#" data-tele="teles-page">
                            <span class="btn downlink">
                                <span class="downlabel">View</span>
                                <span class="downdesc">Telestration</span>
                            </span>
                        </a>
                    </div>
                </div>
                </section>
                <?php endif; ?>
            <section class="hp-sec4">
                <div class="container">

                        <div class="row">
                            <div class="col-md-4">
                                <div class="inner-left-sec">
                                    <div class="content-stub">
                                        <?php the_sub_field('hp_sec_content_left'); ?>
                                    </div>

                                    <?php $attachedgrID = 'attaches_linked_files_left'; ?>
                                    <div class="row downbtns-group">

                                        <?php if( have_rows($attachedgrID) ): ?>
                                            <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                                <?php $url = get_sub_field('english_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('arabic_report'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Report</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                                <?php $url = get_sub_field('english_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">English Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>

                                                <?php $url = get_sub_field('arabic_infographic'); ?>
                                                <?php if($url): ?>
                                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                            <span class="downlabel">Arabic Infographic</span>
                                                            <span class="downdesc">Download</span>
                                                        </a>
                                                    </div>
                                                <?php endif; ?>
                                            <?php endwhile; ?>
                                        <?php endif; ?>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-4 wrapper-middle-sec">
                                <div class="inner-middle-sec">
                                    <?php the_sub_field('hp_sec_content_middle'); ?>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="inner-right-sec">
                                    <div class="content-stub">
                                        <?php the_sub_field('hp_sec_content_right'); ?>
                                    </div>

                                    <?php $attachedgrID = 'attaches_linked_files_right'; ?>
                                    <div class="row downbtns-group">

                                        <?php if( have_rows($attachedgrID) ): ?>
                                        <?php while( have_rows($attachedgrID) ): the_row(); ?>
                                            <?php $url = get_sub_field('english_report'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                    <a class="btn downlink" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">English Report</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <?php $url = get_sub_field('arabic_report'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                    <a class="btn downlink" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">Arabic Report</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                            <?php $url = get_sub_field('english_infographic'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                    <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">English Infographic</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>

                                            <?php $url = get_sub_field('arabic_infographic'); ?>
                                            <?php if($url): ?>
                                                <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-xl-6">
                                                    <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                                        <span class="downlabel">Arabic Infographic</span>
                                                        <span class="downdesc">Download</span>
                                                    </a>
                                                </div>
                                            <?php endif; ?>
                                        <?php endwhile; ?>
                                        <?php endif; ?>

                                    </div>


                                </div>
                            </div>
                        </div>

                    <?php endwhile; ?>
                </div>
            </section>

        <?php endif; ?>
        <section>
            <!-- article -->
            <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                <?php
                    if ( !empty( get_the_content() ) )
                        the_content();
                 ?>
			</article>
			<!-- /article -->

		<?php endwhile; ?>

		<?php else : ?>

			<!-- article -->
			<article>

				<h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

			</article>
			<!-- /article -->

		<?php endif; ?>

		</section>
		<!-- /section -->
	</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
