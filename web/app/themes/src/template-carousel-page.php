<?php /* Template Name: Carousel Page Template */ get_header(); ?>

<main role="main" aria-label="Content">
    <section class="page-inner">

        <div class="container">

            <div class="page-inner-container">
                <div class="page-header-main">
                    <h4 style="top: -40px;position: relative;"><a href="/"><i class="fa fa-angle-left"></i> BACK TO HOME</a></h4>
                    <h4><?php the_title(); ?></h4>
                    <h2><?php the_field('title_long'); ?></h2>
                </div>

                <div class="carousel-wrap-main">


                    <?php

                    $post_object = get_field('slick_carousel');

                    if( $post_object ):

                    // override $post
                    $post = $post_object;
                    setup_postdata( $post );

                    ?>
                            <div class="slick-carousel">
                                <?php
                                    for($i=1;$i<=8;$i++):
                                    $fieldID = 'slide_'.$i;
                                ?>
                                        <?php if( have_rows($fieldID) ): ?>
                                            <?php while( have_rows($fieldID) ): the_row(); ?>
                                                <div>
                                                    <div class="car-image">
                                                        <?php the_sub_field('slide_content'); ?>
                                                    </div>
                                                    <div class="slide-label">
                                                        <?php the_sub_field('slide_label'); ?>
                                                    </div>
                                                </div>
                                            <?php endwhile; ?>
                                        <?php endif; ?>
                                <?php
                                    endfor;
                                    ?>
                            </div>
                        <?php wp_reset_postdata(); // IMPORTANT - reset the $post object so the rest of the page works correctly ?>
                    <?php endif; ?>
                        <?php //get_template_part( 'modules', 'slider' );
                        ?>
                </div>
                <div class="page-content-main">

                    <div class="row">
                        <aside class="col-sm-12 col-md sidebar-buttons order-2 order-md-1">
                            <div class="row">

                                <?php $url = get_field('english_report'); ?>
                                <?php if($url): ?>
                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-lg-12">
                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                            <span class="downlabel">English Report</span>
                                            <span class="downdesc">Download</span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php $url = get_field('arabic_report'); ?>
                                <?php if($url): ?>
                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-lg-12">
                                        <a class="btn downlink" download href="<?php echo $url; ?>">
                                            <span class="downlabel">Arabic Report</span>
                                            <span class="downdesc">Download</span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php $url = get_field('english_infographic'); ?>
                                <?php if($url): ?>
                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-lg-12">
                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                            <span class="downlabel">English Infographic</span>
                                            <span class="downdesc">Download</span>
                                        </a>
                                    </div>
                                <?php endif; ?>

                                <?php $url = get_field('arabic_infographic'); ?>
                                <?php if($url): ?>
                                    <div class="downlinkgrp col-6 col-sm-6 col-md-12 col-lg-12">
                                        <a class="btn downlink gray" download href="<?php echo $url; ?>">
                                            <span class="downlabel">Arabic Infographic</span>
                                            <span class="downdesc">Download</span>
                                        </a>
                                    </div>
                                <?php endif; ?>
                                <?php $iframe = get_field('telestration'); ?>
                                <?php if($iframe): ?>
                                    <div class="col-xs-6 offset-xs-3 col-md-12">
                                        <div class="telestration-overlay justify-content-center align-items-center text-centery" id="teles-page">
                                            <a class="telestration-close" href="#">x</a>
                                            <?php echo $iframe; ?>
                                        </div>
                                        <a class="telestration onpage"  href="#" data-tele="teles-page">
                                            <div class="btn downlink">
                                                <span class="downlabel">View</span>
                                                <span class="downdesc">Telestration</span>
                                            </div>
                                        </a>
                                    </div>
                                <?php endif; ?>

                            </div>


                        </aside>

                        <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                            <!-- article -->
                            <article class="col-sm-12 col-md order-1 order-md-2" id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                                <?php
                                if ( !empty( get_the_content() ) )
                                    the_content();
                                ?>
                            </article>
                            <!-- /article -->

                        <?php endwhile; ?>
                    </div>


                    <?php else : ?>

                        <!-- article -->
                        <article>

                            <h2><?php esc_html_e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

                        </article>
                        <!-- /article -->

                    <?php endif; ?>

                </div>
            </div>

        </div>

    </section>
    <!-- /section -->
</main>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
