# Deployment

Note: This is a Bedrock Wordpress Setup.

Upload files, Note: public directory root should be /web/

## Setup

1. Update environment variables in `.env`  file:
  * `DB_NAME` - Database name
  * `DB_USER` - Database user
  * `DB_PASSWORD` - Database password
  * `DB_HOST` - Database host
  * `WP_ENV` - Set to environment (`development`, `staging`, `production`)
  * `WP_HOME` - Full URL to WordPress home (http://example.com)
  * `WP_SITEURL` - Full URL to WordPress including subdirectory (http://example.com/wp)
  * `AUTH_KEY`, `SECURE_AUTH_KEY`, `LOGGED_IN_KEY`, `NONCE_KEY`, `AUTH_SALT`, `SECURE_AUTH_SALT`, `LOGGED_IN_SALT`, `NONCE_SALT`

  If you want to automatically generate the security keys (assuming you have wp-cli installed locally) you can use the very handy [wp-cli-dotenv-command][wp-cli-dotenv]:
      
      vendor/bin/wp package install aaemnnosttv/wp-cli-dotenv-command

      vendor/bin/wp dotenv salts regenerate

  Or, you can cut and paste from the .env.example file
2. Set your site vhost document root to `/path/to/site/web/` (`/path/to/site/current/web/` if using deploys)

3. `composer install` must be run as part of the deploy process.

4. It should not be required, but you can rebuild the theme at webb/app/themes/src by running `npm install` and `gulp`

5. The database dump can be found at the root of the zip file in the folder "DB_DUMP", make sure you remove the folder before deployment.

6. Note: that you will have to migrate the website domain given the url links for images might be based for the test domain.

7. That's it.

8. Default Admin access: 

www.domain.com/wp-admin

Username: admin
Password: ^J2M6h8yjxKKNx0jsWuOOrCD